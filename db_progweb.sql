DROP DATABASE prog_web;
CREATE DATABASE prog_web;
USE prog_web;

CREATE TABLE product(
    productCode VARCHAR(32) PRIMARY KEY NOT NULL UNIQUE,
    productName VARCHAR(75) NOT NULL UNIQUE,
    sellPrice DECIMAL(10,2) NOT NULL,
    buyPrice DECIMAL(10,2) NOT NULL,
    stock INT,
    INDEX(productCode)
);

CREATE TABLE users(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL UNIQUE,
    username VARCHAR(20) NOT NULL UNIQUE,
    user_pw VARCHAR(32) NOT NULL,
    role VARCHAR(15) NOT NULL,
    INDEX(username)
);


CREATE TABLE payment(
    orderCode INT PRIMARY KEY AUTO_INCREMENT NOT NULL UNIQUE,
    pay_time TIMESTAMP NOT NULL,
    cashierUName VARCHAR(20) NOT NULL,
    INDEX (orderCode),
    FOREIGN KEY (cashierUName) REFERENCES users(username) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE orders(
    orderCode INT NOT NULL,
    productName VARCHAR(75) NOT NULL,
    productCode VARCHAR(32),
    qty SMALLINT(6) NOT NULL,
    priceEach DECIMAL(10,2),
    PRIMARY KEY(orderCode,productName),
    FOREIGN KEY (productCode) REFERENCES product(productCode) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (orderCode) REFERENCES payment(orderCode) ON UPDATE CASCADE ON DELETE RESTRICT
);

DELIMITER //
CREATE TRIGGER decreaseStock AFTER INSERT ON orders
FOR EACH ROW
BEGIN
  UPDATE product
  SET stock = stock - NEW.qty
  WHERE product.productCode = NEW.productCode;
END//
DELIMITER ;

INSERT INTO product VALUES
    ('I1','BENG-BENG',2500,1500,1000),
    ('I2','CHITATO',5000,4000,1000),
    ('I3','PIATOS',6000,5000,1000);

INSERT INTO users (name,username,user_pw,role) VALUES
    ('admin','admin','21232f297a57a5a743894a0e4a801fc3','administrator'),
    ('Dummy1','dummy001','3403ec3627e86920a4328e8c34f301d9','cashier'),
    ('Dummy2','dummy002','3f1ca80eb228777b10f86c2d634f4209','cashier'),
    ('Dummy3','dummy003','d7e7a6b57b3d05c879869bcce25c2a42','cashier');

