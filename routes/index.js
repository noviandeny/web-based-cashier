var express = require("express");
var router = express.Router();
var mysql=require("mysql");
var connection = mysql.createConnection({
  host : "localhost",
  user : "prog_web",
  password : "prog_web",
  database : "prog_web"
  });

 connection.connect();

 // middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    if (!req.session.user) {
        res.redirect("/login");
    } else {
        next();
    }    
};

router.get('/',sessionChecker,function(req,res,next){
    req.session.user != "admin" ? 
        res.redirect("/kasir"):
        res.redirect("/admin");
});

router.get("/logout",function(req,res,next){
    req.session.destroy();
    res.redirect('/');
});

module.exports = router;