var express = require("express");
var router = express.Router();
var mysql=require("mysql");
var connection = mysql.createConnection({
  host : "localhost",
  user : "prog_web",
  password : "prog_web",
  database : "prog_web"
  });

 connection.connect();

// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
  req.session.user ? next() : res.redirect('/');
};
var cart = {};

/* GET home page.*/
router.get('/',sessionChecker, function(req, res, next) {
  req.query.cart ? cart = req.body.cart: cart={};
  
  var nama = [];
  var harga = [];
  var kode = [];

  connection.query("SELECT * FROM product",
    function(err, rows, fields) {
        if(err) throw err;
        for(i=0;i<rows.length;i++)
        {
            nama.push(rows[i].productName);
            harga.push(parseInt(rows[i].sellPrice));
            kode.push(rows[i].productCode);
        }
        res.render("index", {
          title: "Kasir",
          session:req.session,
          productNames: nama,
          prices: harga,
          len: rows.length,
          productCodes: kode,
          cart
        });
  });
});

/* POST from addToCart function */
router.post('/',sessionChecker,function(req,res,next){
  req.body.cart ? cart = JSON.parse(req.body.cart) : cart = {};
  qty = parseInt(req.body.qty);
  deleteStatus = req.body.isDeleted;

  var nama = [];
  var harga = [];
  var kode = [];

  // Validation by checking qty input
  if(qty > 0 || !(deleteStatus)){
    pCode = req.body.pCode;
    pName = req.body.pName;
    price = parseInt(req.body.price);
    cart[pCode] 
    ? cart[pCode].qty+=qty 
    : cart[pCode] = {
      pCode,
      pName,
      price,
      qty
    };
    for(key in cart){
      if(key=="undefined") delete(cart[key]);
    }

    connection.query("SELECT * from product",
      function(err, rows, fields) {
          if(err) throw err;
          for(i=0;i<rows.length;i++)
          {
              nama.push(rows[i].productName);
              harga.push(parseInt(rows[i].sellPrice));
              kode.push(rows[i].productCode);
          }
          res.render("index", {
            session:req.session,
            productNames: nama,
            prices: harga,
            len: rows.length,
            productCodes: kode,
            cart
          });
    });
  } else {
    connection.query("SELECT * from product",
      function(err, rows, fields) {
          if(err) throw err;
          for(i=0;i<rows.length;i++)
          {
              nama.push(rows[i].productName);
              harga.push(parseInt(rows[i].sellPrice));
              kode.push(rows[i].productCode);
          }
          res.render("index", {      
            session:req.session,
            productNames: nama,
            prices: harga,
            len: rows.length,
            productCodes: kode,
            cart,
            msg:"Error input of quantity"
          });
    });

  }
});

/* POST search item */
router.post("/search",sessionChecker, function(req, res, next) {
  var nama = [];
  var harga = [];
  var kode = [];

  var query = `SELECT * FROM product WHERE productName LIKE '%${req.body.searchVal}%'`;
  connection.query(query, function(err, rows, fields) {
        if(err) throw err;
        for(i=0;i<rows.length;i++)
        {
            nama.push(rows[i].productName);
            harga.push(parseInt(rows[i].sellPrice));
            kode.push(rows[i].productCode);
        }
        res.render("index", {
          session:req.session,
          productNames: nama,
          prices: harga,
          len: rows.length,
          productCodes: kode,
          cart:JSON.parse(req.body.cart)
        });
    });
});

/* POST commit */
router.post("/commit",sessionChecker,function(req,res,next){
  var index = [];
  var isValid = true;
  cart = JSON.parse(req.body.cart);

  // biar JSON bisa dipanggil pake index
  // Validation by checking qty input
  for(x in cart){
    if(parseInt(cart[x].qty) > 0){
      index.push(x);
    }
  }

  for(var i = 0; i< index.length;i++){
    if(typeof(parseInt(cart[index[i]].qty)) != "number" || 
    parseInt(cart[index[i]].qty) <= 0) isValid = false;
  }
  if(isValid){
    var query = "INSERT INTO payment (pay_time,cashierUName) VALUES(NOW(),?)";
    var query2= "SELECT LAST_INSERT_ID() INTO @tmp";
    var query3 = "INSERT INTO orders (orderCode,productName,productCode,qty,priceEach) VALUES ";
    for(var i = 0; i< index.length;i++){
      query3 += `(
        @tmp,
        ${mysql.escape(cart[index[i]].pName)},
        ${mysql.escape(cart[index[i]].pCode)},
        ${mysql.escape(parseInt(cart[index[i]].qty))},
        ${mysql.escape(parseInt(cart[index[i]].price))}
        )`;
      i < index.length-1 ? query3+=',': query3+='';
    };
    try{
      connection.query(query,[req.session.user], function (err, result) {
        if(err) throw err;
      });
      connection.query(query2, function (err,rows,result) {
        if(err) throw err;
      });
      connection.query(query3, function (err, result) {
        if(err) throw err;
      });
      cart = {}
      res.redirect('/');
    } catch (err){
      // Cannot insert item
      res.render("index",{
        session:req.session,
        cart:{},
        msg:"Error inserting database"
      });
    }
  } else {
    // TODO : redirect with message
    res.render("index",{
      session:req.session,
      cart:{},
      msg:"Error"
    });
  }
});

/* GET bayar */
router.post("/bayar",sessionChecker,function(req,res,next){
  var bayar = parseInt(req.body.bayar);
  var tagihan = parseInt(req.body.tagihan);
  var total = 0;
  var index = [];
  var cart = JSON.parse(req.body.cart);

  /* Validation by checking qty input*/
  for(x in cart){
    if(parseInt(cart[x].qty) > 0){
      index.push(x);
    }
  }
  
  for(var i = 0; i< index.length;i++){
    total+=parseInt(cart[index[i]].price)*parseInt(cart[index[i]].qty);
  };

  if(bayar >= tagihan){
    res.render("bayar",{
      session:req.session,
      cart,
      total,
      bayar,
      kembali:(bayar-total)
    });
  } else {
    res.render("index",{
      session:req.session,
      cart:cart,
      msg:"Pembayaran minimal harus seharga dengan tagihan"
    });
  }

});

module.exports = router;