var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");

// Define routers
var indexRouter = require("./routes/index");
var kasirRouter = require("./routes/kasir");
var loginRouter = require("./routes/login");
var adminRouter = require("./routes/admin");
var riwayatRouter = require("./routes/riwayat");
var databarangAPI=require("./routes/api/databarang");
var riwayatpaymentAPI= require("./routes/api/riwayatpayment");
var session = require("express-session");

// Define app
var app = express();

// View engine setup
app.set("views", path.join(__dirname, 'views'));
app.set("view engine", "pug");

// Middleware
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({
  key: "userId",
  secret: "somerandonstuffs",
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 3600*2*1000 // milisecond
  }
}));
app.use(express.static('public'))

// Routing
app.use("/api/databarang",databarangAPI);
app.use("/api/riwayatpayment",riwayatpaymentAPI)
app.use("/",indexRouter);
app.use("/login",loginRouter);
app.use("/kasir", kasirRouter);
app.use("/admin",adminRouter);
app.use("/",loginRouter);
app.use("/kasir", kasirRouter);
app.use("/riwayat",riwayatRouter);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.redirect('/');
});

// Error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // Render the error page
  res.status(err.status || 500);
  res.render("error");
});
module.exports = app;
